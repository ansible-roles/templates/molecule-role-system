{{cookiecutter.role_name | upper}}
{% for letter in cookiecutter.role_name %}={% endfor %}

{{ cookiecutter.description }}

Requirements
------------

- Ansible {{cookiecutter.min_ansible_version}}+

Role Variables
--------------

An example variable.

  {{cookiecutter.role_name}}_variable1

Another example variable.

  {{cookiecutter.role_name}}_variable2

Dependencies
------------

None

Example Playbook
----------------

The following example shows how this role can be defined in a playbook with parameters passed to override default variables.

.. code-block:: yaml

    - hosts: servers
      roles:
        - role: {{cookiecutter.role_name}}
          {{cookiecutter.role_name | replace('.','_') | replace('-','_')}}_variable1: true
          {{cookiecutter.role_name | replace('.','_') | replace('-','_')}}_variable2: false
          tags: {{cookiecutter.role_name}}
