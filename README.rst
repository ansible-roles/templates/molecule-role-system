molecule-role
=============

A `cookiecutter` template for setting up a Molecule role.

Usage
=====

::

    $ pip install molecule
    $ molecule init template --url git@code.vt.edu:ansible-roles/templates/molecule-role-system.git
